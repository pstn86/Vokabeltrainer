Unser Project heisst "Vokabeltrainer". 

Die Funktion unseres Vokabeltrainers,
ist das abfragen von Vokabeln. Wobei die Richtigkeit der Antworten berücksichtigt wird. 
Das heißt Falsche und Richtige Vokabeln werden vorgemerkt und werden bei der Abfrage der Vokabeln nach Wunsch separat abgefragt.
Die Vokabeln selbst müssen noch aus einer Textdatei oder DB, Array ausgelesen werden und können nach Wunsch erweitert werden. 
Es werden verschiedene Sprachen zur Auswahl stehen sowie Heimatsprache und Lernsprache zur Auswahl sein.

Technische Herausforderung
Unser größtes Problem wird das Speichern und verwalten der Vokabeln.
Die Vokabeln werden in einer Datenbank abgespeichert und per DB befehle im c Code abgefragt.